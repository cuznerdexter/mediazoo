angular.module('app.services', [])

.service('txtService', ['$http', 
	function($http) {
		console.log('txtService running');

		var txtService = {};

		txtService.loadTxtJson = function() {
			return $http.get('js/textPage.json').
			success(function (data, status, headers, config){
				console.log('success: ',data );
			}).
			error(function (data, status, headers, config){
				console.log('error');
			});
		};
		
		return txtService;
}])


.service('vidService', ['$http',
	function ($http) {
		console.log('vidService running');

		var vidService = {};

		vidService.loadVidJson = function() {
			return $http.get('js/vidPage.json').
			success(function (data, status, headers, config){
				console.log('success: ',data );
			}).
			error(function (data, status, headers, config) {
				console.log('error');
			});
		};

		return vidService;

}])


.service('imgService', ['$http', 
	function ($http) {
		console.log('imgService running');

		var imgService = {};

		imgService.loadImgJson = function () {
			return $http.get('js/intPage.json').
			success(function (data, status, headers, config) {
				console.log('success: ',data);
			}).
			error(function (data, status, headers, config) {
				console.log('error');
			});
		};

		return imgService;
}])

.service('scoreService', [
	function () {
		var scoreService = {};
		scoreService.score = 0;

		scoreService.getScore = function () {
			return scoreService.score;
		};

		scoreService.setScore = function (newScore) {
			scoreService.score = scoreService.score + newScore;
		};

		return scoreService;
}])

