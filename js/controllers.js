angular.module('app.controllers', [])

.controller('mainController', ['$state', '$scope', 
	function($state, $scope) {
	console.log('mainController:');
	console.log($state);

	$scope.appPrev = 'prev';
	$scope.appNext = 'next';
	$scope.appCurr = 'start';
	$scope.currState = 'first';
	$scope.hideLeftButton = 'false';
	$scope.hideRightButton = 'false';

	$scope.$on('UPDATE', function (event, newData) {
		$scope.appPrev = newData.prev;
		$scope.appNext = newData.next;
		$scope.appCurr = newData.curr;
		$scope.currState = newData.currState;

		switch ($scope.currState) {
			case 'txt':
				$scope.hideLeftButton = true;
				$scope.hideRightButton = false;
				break;
			case 'int':
				$scope.hideLeftButton = false;
				$scope.hideRightButton = true;
				break;
			default:
				$scope.hideLeftButton = false;
				$scope.hideRightButton = false;


		}
		console.log('update hit', $scope.appPrev, '+', $scope.appCurr, '+', $scope.appNext, '+', $scope.currState );
	})

	$scope.setPage = function (direction) {
		console.log(direction);
		if (direction == 'txt') {
			console.log('hit txt');
			$state.go('txt');
		}
		else if (direction == 'vid') {
			console.log('hit vid');
			$state.go('vid');
		}
		else if (direction == 'int') {
			console.log('hit int');
			$state.go('int');
		}
	};
	

}])

.controller('txtController', ['$state', '$scope', 'loadTxtData',
	function($state, $scope, loadTxtData) {
	console.log('txtController:');

	$scope.txtList = loadTxtData.data.textSet;

	var navData = {
		prev : 'txt',
		next : 'vid',
		curr : 'txt',
		currState: $state.current.name
	}
	$scope.$emit('UPDATE', navData);
	

	
}])

.controller('vidController', ['$state', '$scope', 'loadVidData',
	function($state, $scope, loadVidData) {
	console.log('vidController:');

	$scope.vidList = loadVidData.data.vidSet;
	console.log($scope.vidList);
	
	var navData = {
		prev : 'txt',
		next : 'int',
		curr : 'vid',
		currState: $state.current.name
	}
	$scope.$emit('UPDATE', navData);
	
}])

.controller('intController', ['$state', '$scope', 'updateScore', /*'loadImgData',*/
	function($state, $scope, updateScore/*, loadImgData*/) {
	console.log('intController:');

	//$scope.imgList = loadImgData.data.intSet;
	//console.log($scope.imgList);

	//$scope.backImg = $scope.imgList.int1.imgName;
	$scope.currScore = updateScore.score;

	$scope.$watch('currScore', function(newValue, oldValue) {
  		$scope.currScore = updateScore.score;	
	});	

	$scope.add1 = function () {
		updateScore.setScore(1);
		$scope.currScore = updateScore.score;
		
	}
	$scope.add5 = function () {
		updateScore.setScore(5);
		$scope.currScore = updateScore.score;
		
	}
	$scope.add10 = function () {
		updateScore.setScore(10);
		$scope.currScore = updateScore.score;
		
	}
	
	

	
	
	var navData = {
		prev : 'vid',
		next : 'int',
		curr : 'int',
		currState: $state.current.name
	}
	$scope.$emit('UPDATE', navData);
	
}]);


