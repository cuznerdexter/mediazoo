

var app = angular.module('app', ['ui.router', 'ngAnimate', 'app.directives', 'app.controllers', 'app.services'])

//console.log(app);
app.config( ['$stateProvider', '$urlRouterProvider', 
	function($stateProvider, $urlRouterProvider) {
		$stateProvider
		.state('home', {
			controller: 'mainController',
			templateUrl: 'templates/home.html'
			
		})
		.state('txt', {
			url:'/txt',
			controller: 'txtController',
			templateUrl: 'templates/txt.html',
			resolve: {
				loadTxtData: function (txtService) {
					return txtService.loadTxtJson();
				}
			}
			
		})
		.state('vid', {
			url:'/vid',
			controller: 'vidController',
			templateUrl: 'templates/vid.html',
			resolve: {
				loadVidData: function (vidService) {
					return vidService.loadVidJson();
				}
			}
			
		})
		.state('int', {
			url:'/int',
			controller: 'intController',
			templateUrl: 'templates/int.html',
			resolve: {
				/*loadImgData: function (imgService) {
					return imgService.loadImgJson();
				},*/
				updateScore: function (scoreService) {
					return scoreService;
				}
			}
			
		});
		$urlRouterProvider.when('', '/txt');
		$urlRouterProvider.when('/', '/txt');
		$urlRouterProvider.otherwise('/txt');
}])



.run(['$state',
	function($state){
		console.log('running');
		$state.go('txt');

}])




