angular.module('app.directives', [])

.directive('mediaElem', [ function() {
	console.log('directive: mediaElem: running');

	return function (scope, elem, attr) {
		console.log(scope, elem, attr);
		var player;
		$(document).ready(function () {
			console.log('READY');
			
			var vidList = scope.vidList;
			var currVid = vidList.vid1.title;
			
			player = new MediaElementPlayer(elem, {

				features: ['playpause', 'progress', 'duration', 'volume', 'fullscreen'],
				type: ['video/mp4', 'video/ogg', 'video/webm'],
				success: function (mediaElement) {
					console.log('success:');
					var sources = [
						{ src: "vid/" + currVid + "/" + currVid + ".mp4", type: 'video/mp4' },
						{ src: "vid/" + currVid + "/" + currVid + ".ogv", type: 'video/ogg' },
						{ src: "vid/" + currVid + "/" + currVid + ".webm", type: 'video/webm' }
					];
					mediaElement.setSrc(sources);
					mediaElement.pause();
					mediaElement.load();
				},
				error: function (err) {
					console.log('Error: ', err);
				}
			});
		});


		
	}
}])



/*var player = new MediaElementPlayer(elem, {
			alwaysShowControls: true
		});
		player.pause();
		player.setSrc('vid/SpaceDogJellyZapMain_14/SpaceDogJellyZapMain_14.mp4');
		player.play();*/